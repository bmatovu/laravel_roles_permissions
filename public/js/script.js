$(function () {

    $('div.alert').not('.alert-danger, .alert-important').delay(3000).fadeOut(350);

    // Every time a modal is shown, if it has an autofocus element, focus on it.
    $('.modal').on('shown.bs.modal', function () {
        $(this).find('[autofocus]').focus();
    });

    $('#edit-role-modal').on('show.bs.modal', function () {

        var panel = $(event.target).closest('.panel');

        var id = panel.data('role-id');
        $(this).find('input[name=id]').val(id);

        var name = panel.data('role-name');
        $(this).find('input[name=name]').val(name);

    });

    $('#drop-role-modal').on('show.bs.modal', function () {

        var panel = $(event.target).closest('.panel');

        var id = panel.data('role-id');
        $(this).find('#role-id').val(id);

        var name = panel.data('role-name');
        $(this).find('#role-name').html(name);

    });

    $('#edit-permission-modal').on('show.bs.modal', function () {

        var row = $(event.target).closest('tr');
        var perm_id = 0;

        row.find('td').each(function () {
            var bind = $(this).data('bind');
            var target = $('#edit-permission-modal').find('#' + bind);

            if (bind === 'perm-id') {
                perm_id = $(this).html();
                target.val($(this).html());
            } else if (bind === 'perm-roles') {
                // ignore
            } else {
                target.val($(this).html());
            }

        });

        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: app + '/permissions/' + perm_id + '/roles',
            success: function (perm) {
                var form = modal.find('form#edit-permission');
                form.find('.checkbox').each(function () {
                    var role = $(this).find('input#perm-roles');
                    role.attr('checked', false);
                    if ($.inArray(parseInt(role.val()), perm.roles) > -1) {
                        role.attr('checked', true);
                    }
                });
            },
            error: function (xhr) {
                console.error(xhr.responseText);
            }
        });

    });

    $('#drop-permission-modal').on('show.bs.modal', function () {

        var row = $(event.target).closest('tr');

        row.find('td').each(function () {
            var bind = $(this).data('bind');
            var target = $('#drop-permission-modal').find('#' + bind);
            if (target.is('input')) {
                target.val($(this).html()); // input
            } else {
                target.html($(this).html()); // span
            }
        });

    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

$(document).ready(function () {

    $('form#add-role').on('submit', function (event) {

        event.preventDefault();

        var form = $(this);

        $.ajax({
            type: 'POST',
            url: app + '/roles',
            data: form.serialize(),
            success: function (response) {
                console.log(response);
                window.location = app + '/roles';
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors, function (param, error) {

                    var form_group = form.find('input[name=' + param + ']').closest('div.form-group');
                    form_group.addClass('has-error');

                    var error_msg = '<span class="help-block"><strong>' + error[0] + '</strong></span>';
                    if (form_group.find('.help-block')[0]) {
                        form.find('.help-block').remove();
                    }
                    form_group.append(error_msg);

                });

            }
        });

    });

    $('form#edit-role').on('submit', function (event) {

        event.preventDefault();

        var form = $(this);

        var role_id = form.find('#role-id').val();

        $.ajax({
            type: 'POST',
            url: app + '/roles/' + role_id,
            data: form.serialize(),
            success: function (response) {
                console.log(response);
                window.location = app + '/roles';
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors, function (param, error) {

                    var form_group = form.find('input[name=' + param + ']').closest('div.form-group');
                    form_group.addClass('has-error');

                    var error_msg = '<span class="help-block"><strong>' + error[0] + '</strong></span>';
                    if (form_group.find('.help-block')[0]) {
                        form.find('.help-block').remove();
                    }
                    form_group.append(error_msg);

                });

            }
        });

    });

    $('form#drop-role').on('submit', function (event) {

        event.preventDefault();

        var form = $(this);

        var role_id = form.find('#role-id').val();

        $.ajax({
            type: 'POST',
            url: app + '/roles/' + role_id,
            data: form.serialize(),
            success: function (response) {
                console.log(response);
                window.location = app + '/roles';
            },
            error: function (xhr) {
                console.error(xhr.responseText);
            }
        });

    });

    $('form#add-permission').on('submit', function (event) {

        event.preventDefault();

        var form = $(this);

        $.ajax({
            type: 'POST',
            url: app + '/permissions',
            data: form.serialize(),
            success: function (response) {
                console.log(response);
                window.location = app + '/permissions';
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors, function (param, error) {

                    var form_group = form.find('input[name=' + param + ']').closest('div.form-group');
                    form_group.addClass('has-error');

                    var error_msg = '<span class="help-block"><strong>' + error[0] + '</strong></span>';
                    if (form_group.find('.help-block')[0]) {
                        form.find('.help-block').remove();
                    }
                    form_group.append(error_msg);

                });

            }
        });

    });

    $('form#edit-permission').on('submit', function (event) {

        event.preventDefault();

        var form = $(this);

        var perm_id = form.find('#perm-id').val();

        $.ajax({
            type: 'POST',
            url: app + '/permissions/' + perm_id,
            data: form.serialize(),
            success: function (response) {
                console.log(response);
                window.location = app + '/permissions';
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors, function (param, error) {

                    var form_group = form.find('input[name=' + param + ']').closest('div.form-group');
                    form_group.addClass('has-error');

                    var error_msg = '<span class="help-block"><strong>' + error[0] + '</strong></span>';
                    if (form_group.find('.help-block')[0]) {
                        form.find('.help-block').remove();
                    }
                    form_group.append(error_msg);

                });

            }
        });

    });

    $('form#drop-permission').on('submit', function (event) {

        event.preventDefault();

        var form = $(this);

        var perm_id = form.find('input[name=id]').val();

        $.ajax({
            type: 'POST',
            url: app + '/permissions/' + perm_id,
            data: form.serialize(),
            success: function (response) {
                console.log(response);
                window.location = app + '/permissions';
            },
            error: function (xhr) {
                console.error(xhr.responseText);
            }
        });

    });

});