## [Laravel Api Token Based Authentication](https://www.google.com) ##

### 1. Install required packages ###

2.1. [laracasts/flash](https://packagist.org/packages/laracasts/flash) :: Easy flash notifications

`composer require laracasts/flash`

Note: Requires Bootstrap

`<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">`

Register service provider within your `config/app.php`
```
'providers' => [
    // ...
    Laracasts\Flash\FlashServiceProvider::class,
];
```

2.2. [spatie/laravel-permission](https://packagist.org/packages/spatie/laravel-permission) :: Permission handling for Laravel 
5.4 
and up

`composer require spatie/laravel-permission`

Register service provider within your `config/app.php`
```
'providers' => [
    // ...
    Spatie\Permission\PermissionServiceProvider::class,
];
```

Publish the migration with:

`php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"`

After the migration has been published you can create the role- and permission-tables by running the migrations:

`php artisan migrate`

Publish the config file with:

`php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"`

This will publish `config/permission.php` config file:

### 2. Step up prerequisites ###

[* Step up default string length (Only for MariaDB)](https://laravel.com/docs/master/migrations#creating-indexes)
`Error: Syntax error or access violation: 1071 Specified key was too 
long; max key length is 767 bytes`

`app/Providers/AppServiceProvider.php`

```
public function boot()
{
    Schema::defaultStringLength(191);
}
```

> Run default Laravel Authentication

`php artisan make:auth`

Migrate database

`php artisan migrate`

Seed dummy data

`php artisan db:seed`
