@extends('layouts.app')

@section('title', '| Roles')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <div class="col-md-6">
                <h4><strong>Roles</strong></h4>
            </div>
            <div class="col-md-6 page-action text-right">
                <a href="#" class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#add-role-modal">
                    <i class="glyphicon glyphicon-plus"></i> Add Role</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @foreach($roles as $role)
                    <form role="form" method="POST" action="{{ route('roles.permissions.update', $role->id) }}">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="panel panel-default" data-role-id="{{ $role->id }}" data-role-name="{{ $role->name }}">
                            <div role="tab" id="{{ $role->name }}-permissions" class="panel-heading">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#dd-{{ $role->name }}-permissions"
                                   aria-expanded="true" aria-controls="dd-{{ $role->name }}-permissions">
                                    <span class="caret"></span>
                                    <strong>{{ $role->name }} Role :: </strong>Pemissions ({{ $role->permissions->count() }})
                                </a>
                            <span class="pull-right">
                                <input type="submit" class="btn btn-success btn-sm" value="Save">
                                <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-role-modal">Edit</a>
                                <a href="#"  class="btn btn-danger btn-sm" data-toggle="modal" data-target="#drop-role-modal">Drop</a>
                            </span>
                            </div>
                            <div id="dd-{{ $role->name }}-permissions" role="tabpanel" aria-labelledby="dd-{{ $role->name }}-permissions"
                                 class="panel-collapse collapse out" aria-expanded="false">
                                <div class="panel-body">
                                    @foreach($permissions as $perm)
                                        <div class="form-group col-md-4 col-md-3 col-lg-2">
                                            <div class="checkbox">
                                                <label class="">
                                                    <input name="permissions[]" type="checkbox" value="{{ $perm->name }}"
                                                        {{ $role->hasPermissionTo($perm->name) ? 'checked' : '' }}> {{ $perm->name }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>
        </div>

    </div>

    {{-- Add Role Modal --}}
    <div class="modal fade" id="add-role-modal" tabindex="-1" role="dialog" aria-labelledby="addRoleModalLabel">

        <div class="modal-dialog" role="document">

            <form id="add-role" method="POST" action="{{ route('roles.store') }}" role="form">

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="addRoleModalLabel">Add Role</h4>
                    </div>

                    <div class="modal-body">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="control-label">Role Name</label>
                            <input id="name" type="text" class="form-control" name="name" value="" required autofocus>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-success btn-sm" value="Add">
                    </div>

                </div>

            </form>

        </div>

    </div>

    {{-- Edit Role Modal --}}
    <div class="modal fade" id="edit-role-modal" tabindex="-1" role="dialog" aria-labelledby="editRoleModalLabel">

        <div class="modal-dialog" role="document">

            <form id="edit-role" method="POST" action="{{ route('roles.update', $role->id) }}" role="form">

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="editRoleModalLabel">Edit Role</h4>
                    </div>

                    <div class="modal-body">

                        {{ csrf_field() }}

                        {{ method_field('PUT') }}

                        <input id="role-id" type="hidden" name="id" value="">

                        <div class="form-group">
                            <label for="name" class="control-label">Role Name</label>
                            <input id="name" type="text" class="form-control" name="name" value="" required autofocus>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-success btn-sm" value="Update">
                    </div>

                </div>

            </form>

        </div>

    </div>

    {{-- Drop Role Modal --}}
    <div class="modal fade" id="drop-role-modal" tabindex="-1" role="dialog" aria-labelledby="dropRoleModalLabel">

        <div class="modal-dialog" role="document">

            <form id="drop-role" method="POST" action="{{ route('roles.destroy', $role->id) }}" data-role-id="" role="form">

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="addRoleModalLabel">Drop Role</h4>
                    </div>

                    <div class="modal-body">

                        <div class="alert alert-danger">
                            Are you sure you want to drop the <strong>'<span id="role-name"></span>' </strong>role?
                        </div>

                        {{ csrf_field() }}

                        {{ method_field('DELETE') }}

                        <input id="role-id" type="hidden" name="id" value="">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-danger btn-sm" value="Drop">
                    </div>

                </div>

            </form>

        </div>

    </div>

@endsection