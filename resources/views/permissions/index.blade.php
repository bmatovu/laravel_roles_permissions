@extends('layouts.app')

@section('title', '| Permissions')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span><strong>Permissions</strong></span>
                        <span class="pull-right">
                            <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add-permission-modal">Add Permission</a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">

                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="hidden">ID</th>
                                    <th>Permission</th>
                                    <th>Roles</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($permissions as $index => $perm)
                                    <tr>
                                        <td>{{ ++$index }}</td>
                                        <td data-bind="perm-id" class="hidden">{{ $perm->id }}</td>
                                        <td data-bind="perm-name">{{ $perm->name }}</td>
                                        <td data-bind="perm-roles">{{--[{{ $perm->roles->count() }}]--}} {{ $perm->roles->implode('name', ', ') }}</td>
                                        <td class="text-center">
                                            <a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#edit-permission-modal"
                                               style="margin-top: 2px;">
                                                <i class="glyphicon glyphicon-pencil"></i> Edit
                                            </a>
                                            <a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#drop-permission-modal"
                                               style="margin-top: 2px;">
                                                <i class="glyphicon glyphicon-trash"></i> Drop
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Add Permission Modal --}}
    <div class="modal fade" id="add-permission-modal" tabindex="-1" role="dialog" aria-labelledby="addPermissionModalLabel">

        <div class="modal-dialog" role="document">

            <form id="add-permission" method="POST" action="{{ route('permissions.store') }}" role="form">

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="addPermissionModalLabel">Add Permission</h4>
                    </div>

                    <div class="modal-body">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input id="name" type="text" class="form-control" name="name" value="" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="name" class="control-label">Assign to...</label>
                            @foreach($roles as $role)
                                <div class="checkbox">
                                    <label class="">
                                        <input name="roles[]" type="checkbox" value="{{ $role->id }}"> {{ $role->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-success btn-sm" value="Add">
                    </div>

                </div>

            </form>

        </div>

    </div>

    {{-- Edit Permission Modal --}}
    <div class="modal fade" id="edit-permission-modal" tabindex="-1" role="dialog" aria-labelledby="editPermissionModalLabel">

        <div class="modal-dialog" role="document">

            <form id="edit-permission" method="POST" action="{{ route('permissions.update', $perm->id) }}" role="form">

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="editPermissionModalLabel">Edit Permission</h4>
                    </div>

                    <div class="modal-body">

                        {{ csrf_field() }}

                        {{ method_field('PUT') }}

                        <input id="perm-id" type="hidden" name="id" value="">

                        <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input id="perm-name" type="text" class="form-control" name="name" value="" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="name" class="control-label">Belong to...</label>
                            @foreach($roles as $role)
                                <div class="checkbox">
                                    <label class="">
                                        <input id="perm-roles" name="roles[]" type="checkbox" value="{{ $role->id }}"> {{ $role->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-success btn-sm" value="Update">
                    </div>

                </div>

            </form>

        </div>

    </div>

    {{-- Drop Permission Modal --}}
    <div class="modal fade" id="drop-permission-modal" tabindex="-1" role="dialog" aria-labelledby="dropPermissionModalLabel">

        <div class="modal-dialog" role="document">

            <form id="drop-permission" method="POST" action="{{ route('permissions.destroy', $perm->id) }}" data-role-id="" role="form">

                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="addPermissionModalLabel">Drop Permission</h4>
                    </div>

                    <div class="modal-body">

                        <div class="alert alert-danger">
                            Are you sure you want to drop the <strong>'<span id="perm-name"></span>' </strong>permission?
                        </div>

                        {{ csrf_field() }}

                        {{ method_field('DELETE') }}

                        <input id="perm-id" type="hidden" name="id" value="">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-danger btn-sm" value="Drop">
                    </div>

                </div>

            </form>

        </div>

    </div>

@endsection