@extends('layouts.app')

@section('title', '| Users')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span><strong>Users</strong></span>
                        <span class="pull-right">
                            <a href="{{ route('users.create') }}" class="btn btn-success btn-sm">Add User</a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">

                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Added</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->roles->implode('name', ', ') }}</td>
                                        <td>{{ $user->created_at->format('F d, Y H:i') }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info btn-xs pull-left" style="margin-top: 2px;">
                                                <i class="glyphicon glyphicon-pencil"></i> Edit
                                            </a>

                                            <form method="POST" action="{{ route('users.destroy', $user->id) }}"
                                                  onSubmit="return confirm('Are yous sure wanted to delete it?')">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-delete btn-xs btn-danger">
                                                    <i class="glyphicon glyphicon-trash"></i> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection