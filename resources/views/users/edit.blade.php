@extends('layouts.app')

@section('title', 'Edit User ' . $user->name)

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Editing :: </strong> {{$user->name}}</div>
                    <div class="panel-body">
                        <form class="col-md-12" role="form" method="POST" action="{{ route('users.update', $user->id) }}">

                            {{ method_field('PUT') }}

                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="control-label">Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $user->name }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" readonly>
                            </div>

                            <div class="form-group @if ($errors->has('roles')) has-error @endif">
                                <label for="email" class="control-label">Roles</label>
                                @foreach ($roles as $id => $name)
                                    <div class="checkbox">
                                        <label><input id="roles[]" type="checkbox" name="roles[]" {{ in_array($id, $user_roles) ? 'checked' : '' }} value="{{ $id }}">{{ $name }}</label>
                                    </div>
                                @endforeach
                                @if ($errors->has('roles'))
                                    <p class="help-block">{{ $errors->first('roles') }}</p>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">New Password</label>
                                <input id="password" type="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password-confirmation" class="control-label">Confirm Password</label>
                                <input id="password-confirmation" type="password" class="form-control" name="password_confirmation">
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Update User</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection