<?php

use App\Role;
use App\Permission;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if ($this->command->confirm('Do you wish to refresh migration; clearly all existing db data?')) {
            // Call the php artisan migrate:refresh
            $this->command->call('migrate:refresh');
            $this->command->warn("DB cleared, starting from blank database.");
        }

        $input_perms = $this->command->ask('Enter permission(s) (comma separated). ', 'users.manage');
        $perms = explode(',', $input_perms);
        foreach ($perms as $perm) {
            $perm = Permission::firstOrCreate(['name' => trim($perm)]);
        }
        $this->command->info('Created permissions: ' . $input_perms);

        $input_roles = $this->command->ask('Enter role(s) (comma separated). ', 'admin');
        $roles = explode(',', $input_roles);
        foreach ($roles as $role) {

            $role = Role::firstOrCreate(['name' => trim($role)]);

            if ($role->name == 'admin') {
                // assign all permissions
                $role->syncPermissions(Permission::all());
                $this->command->info('Admin granted all the permissions');
            } else {
                // for others by default only read access
                $role->syncPermissions(Permission::where('name', 'LIKE', 'view_%')->get());
            }

            // create one user for each role
            $this->createUser($role);

        }

        $this->command->info('Created roles: ' . $input_roles . ' & assigned permissions to the roles');

    }

    /**
     * Create a user with given role
     *
     * @param $role
     */
    private function createUser($role)
    {
        $user = factory(User::class)->create();
        $user->assignRole($role->name);

        $this->command->info('>>>>>>>>>> Users account: <<<<<<<<<<');
        $this->command->warn('Email: "'.$user->email.'"');
        $this->command->warn('Role: "'.$role->name.'"');
        $this->command->warn('Password: "secret"');
    }

}
