<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::get('users/{user}/profile', 'UserController@profile')->name('users.profile');
    Route::resource('users', 'UserController');
    Route::resource('posts', 'PostController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::put('roles/{role}/permissions', 'RoleController@updatePermissions')->name('roles.permissions.update');
    Route::get('permissions/{permission}/roles', 'PermissionController@getRoles')->name('permissions.roles.view');
    Route::post('permissions/{permission}/roles', 'PermissionController@updateRoles')->name('permissions.roles.update');
});