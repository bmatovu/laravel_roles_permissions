<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $permissions = Permission::all();
        return view('permissions.index', compact('permissions', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('permissions.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, ['name' => 'required|max:40|unique:permissions']);

        if ($permission = Permission::create($request->only('name'))) {

            if ($request->has('roles')) {
                $roles = Role::find($request->get('roles', []));
                foreach ($roles as $role) {
                    $role->givePermissionTo($permission);
                }
            }

            flash('Permission: ' . $request->name . ' added');
        } else {
            flash('Unknown Permission');
        }

        if ($request->ajax()) {
            return response()->json(['message' => 'Permission: ' . $request->name . ' added']);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('permissions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);

        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required|max:40']);

        if ($perm = Permission::find($id)) {

            $perms = Permission::get()->where('name', $request->name)->pluck('name');

            if ($request->name != $perm->name && in_array($request->name, $perms->toArray())) {
                return response()->json(['name' => ['Permission: \'' . $request->name . '\' is already in-use']], 422);
            } else {
                // update permission name
                $perm->fill($request->except('roles'))->save();

                // Revoke this permission from all existing roles...
                $roles = Role::get();
                foreach ($roles as $role) {
                    $role->revokePermissionTo($perm);
                }

                // Re-assign permission to only selected roles...
                $roles = Role::find($request->get('roles', []));
                foreach ($roles as $role) {
                    $role->givePermissionTo($perm);
                }

                flash('Permission: ' . $perm->name . ' updated!');
            }

        } else {
            flash()->error('Unknown permission.');
        }

        if ($request->ajax()) {
            return response()->json(['message' => 'Permission: ' . $request->name . ' added']);
        }

        return redirect()->route('permissions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);

        $permission->delete();

        flash('Permission has been dropped!');

        if ($request->ajax()) {
            return response()->json(['message' => 'Permission has been dropped']);
        }

        return redirect()->route('permissions.index');
    }

    /**
     * Get roles having this permission
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoles(Request $request, $id)
    {

        if (!$permission = Permission::find($id)) {
            return response()->json(['error' => 'Unknown permission'], 201);
        }

        if ($request->ajax()) {
            return response()->json(['roles' => $permission->roles->pluck('id')]);
        }

        return response()->json(['roles' => $this->array_extract($permission->roles->toArray(), ['id', 'name'])]);
    }

    /**
     * Extract subset of array using keys
     * @param $array
     * @param $keys
     * @return array
     */
    private function array_extract($roles, $keys)
    {
        // Source: https://stackoverflow.com/a/569502
        $roles_subset = array();
        foreach ($roles as $role) {
            array_push($roles_subset, array_intersect_key($role, array_flip(['id', 'name'])));
        }
        return $roles_subset;
    }

}