<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    public function __construct(){
        # $this->middleware('authorize:'); # =>['role'=>'Admin|Seller']
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $permissions = Permission::all();
        return view('roles.index', compact('roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:roles']);

        if (Role::create($request->only('name'))) {
            flash('Role Added');
        }

        // https://stackoverflow.com/a/37724030/2732184
        if ($request->ajax()) {
            return response()->json(['message' => 'Role added']);
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required|unique:roles']);

        if ($role = Role::findOrFail($id)) {
            // update role name...
            $role->fill($request->except('permissions'))->save();

            flash($role->name . ' role has been updated.');
        } else {
            flash()->error('Unknown role.');
        }

        if ($request->ajax()) {
            return response()->json(['message' => $role->name . ' role has been update']);
        }

        return redirect()->route('roles.index');
    }

    /**
     * Update the role permissions
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updatePermissions(Request $request, $id)
    {

        if ($role = Role::findOrFail($id)) {
            // admin role has everything
            if ($role->name === 'Admin') {
                $role->syncPermissions(Permission::all());
                return redirect()->route('roles.index');
            }

            $permissions = $request->get('permissions', []);

            $role->syncPermissions($permissions);

            flash($role->name . ' permissions has been updated.');
        } else {
            flash()->error('Unknown role.');
        }

        if ($request->ajax()) {
            return response()->json(['message' => $role->name . ' role has been update']);
        }

        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        flash($role->name . ' role has been dropped');

        if ($request->ajax()) {
            return response()->json(['message' => $role->name . ' role has been dropped']);
        }

        return redirect()->route('roles.index');
    }

}