<?php

namespace App\Http\Middleware;

use Closure;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

class CheckPermission
{

    /*
     * Usage:
     * Route::get('/home', 'HomeController@index')->middleware('perm:perm_1|perm_2|...|perm_n');
     * Accepts multiple permissions with "|" delimiter
     *
     * Pending:
     * Permission enforcement with guards
     */

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {

        $authorized = false;

        $permissions = explode("|", $permission);

        try {

            # Get user roles...
            $roles = $request->user()->roles;
            foreach ($roles as $role) {

                # If any of the user roles has any of the required permissions; stop...
                if ($authorized) {
                    break;
                }

                # Check if user roles has any of the required permissions
                foreach ($permissions as $permission) {

                    if ($role->hasPermissionTo($permission)) {
                        $authorized = true;
                        # If hasAny of the required permissions
                        break; # Remove to enforce all permissions
                    }

                }

            }

        } catch (PermissionDoesNotExist $ex) {
            abort(500, "Unknown permission ");
        }

        if (!$authorized) {

            if($request->expectsJson() || $request->ajax()){
                return response()->json(['error'=>'Forbidden; You\'re not permitted to access this resource'], 403);
            }

            abort(403, "Forbidden");

        }

        return $next($request);
    }

}
